#!/bin/sh

OUTPATH="$(pwd)/src/bin"
echo $OUTPATH

cd src

mkdir -p bin
rm bin/jrery-bot-go-*

echo "Building linux-amd64"
GOOS=linux GOARCH=amd64 go build -o "$OUTPATH/jrery-bot-go-linux-amd64" ./cmd/jrer-bot-go/main.go
echo "Building linux-386"
GOOS=linux GOARCH=386 go build -o "$OUTPATH/jrery-bot-go-linux-386" ./cmd/jrer-bot-go/main.go
echo "Building linux-arm"
GOOS=linux GOARCH=arm go build -o "$OUTPATH/jrery-bot-go-linux-arm" ./cmd/jrer-bot-go/main.go
echo "Building linux-arm64"
GOOS=linux GOARCH=arm64 go build -o "$OUTPATH/jrery-bot-go-linux-arm" ./cmd/jrer-bot-go/main.go

echo "Building win-amd64"
GOOS=windows GOARCH=amd64 go build -o "$OUTPATH/jrery-bot-go-win-amd64" ./cmd/jrer-bot-go/main.go
echo "Building win-386"
GOOS=windows GOARCH=386 go build -o "$OUTPATH/jrery-bot-go-win-386" ./cmd/jrer-bot-go/main.go

echo "Building android-arm64"
GOOS=android GOARCH=arm64 go build -o "$OUTPATH/jrery-bot-android-arm64" ./cmd/jrer-bot-go/main.go

echo "Finished"