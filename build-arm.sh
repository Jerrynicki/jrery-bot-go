#!/bin/sh

OUTPATH="$(pwd)/src/bin"
echo $OUTPATH

cd src

mkdir -p bin
rm bin/jrery-bot-go-*

echo "Building linux-arm"
GOOS=linux GOARCH=arm go build -o "$OUTPATH/jrery-bot-go-linux-arm" ./cmd/jrer-bot-go/main.go
echo "Building linux-arm64"
GOOS=linux GOARCH=arm64 go build -o "$OUTPATH/jrery-bot-go-linux-arm" ./cmd/jrer-bot-go/main.go
