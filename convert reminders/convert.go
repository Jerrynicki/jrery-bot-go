package main

import (
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

const (
	remindersFileOld = "reminders.json"
	remindersFileNew = "reminders.gob"
)

func main() {
	var reminders []utils.Reminder

	oldData, err := ioutil.ReadFile(remindersFileOld)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(oldData, &reminders)
	if err != nil {
		panic(err)
	}

	fileNew, err := os.Create(remindersFileNew)
	if err != nil {
		panic(err)
	}

	encoder := gob.NewEncoder(fileNew)
	encoder.Encode(&reminders)
	fileNew.Close()

	fmt.Println("Success!\nVerification:")

	fmt.Println("Old data:", reminders)

	fileConfirm, err := os.Open(remindersFileNew)
	if err != nil {
		panic(err)
	}

	decoder := gob.NewDecoder(fileConfirm)
	decoder.Decode(&reminders)
	fileConfirm.Close()

	fmt.Println("New data:", reminders)
}
