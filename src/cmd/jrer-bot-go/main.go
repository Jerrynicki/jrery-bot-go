package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/commands"
	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/slashcmds"
	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

func main() {
	var discord *discordgo.Session

	// Seed random generator
	// not necessary as of go 1.20
	// rand.Seed(time.Now().Unix())

	// Initialize config
	utils.ReadConfig()

	// Create discord session
	fmt.Println("Connecting to discord and adding handlers.")
	discord, err := discordgo.New("Bot " + utils.Config.Token)
	if err != nil {
		panic(err)
	}

	// Add event handlers
	discord.AddHandler(ready)
	discord.AddHandler(commands.MessageCreate)
	discord.AddHandler(slashcmds.InteractionCreate)

	// Add intents
	discord.Identify.Intents = discordgo.IntentsGuilds | discordgo.IntentsGuildMessages | discordgo.IntentsGuildVoiceStates

	// Open websocket for listening
	err = discord.Open()
	if err != nil {
		panic(err)
	}

	// Create channel to notify if bot is closed by either Ctrl+C or SIGTERM
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

	// Wait for message in said channel, then shut down
	<-sc
	fmt.Println("\njrery bot shutting down, увидимся!!")

	discord.Close()
}

func ready(discord *discordgo.Session, event *discordgo.Ready) {
	fmt.Println("Ready!!!")

	// Initialize modules
	commands.CommandsHelpInit()
	utils.RemindInit(discord)

	// Slash commands
	slashcmds.UnregisterAllSlashCmds(discord)
	slashcmds.RegisterSlashCmds(discord)

	// Set status
	discord.UpdateGameStatus(0, utils.Config.Prefix+" | Report issues at gitlab.com/Jerrynicki/jrery-bot-go")
}
