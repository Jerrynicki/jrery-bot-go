package commands

import "github.com/bwmarrin/discordgo"

func CommandPing(discord *discordgo.Session, message *discordgo.MessageCreate, args []string) {
	// The ping command, for more info see help.txt
	discord.ChannelMessageSend(message.ChannelID, "Pong <a:peepoHehee:881101604554694676>")
}
