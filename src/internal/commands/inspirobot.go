package commands

import (
	"io/ioutil"
	"net/http"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

const (
	inspirobotAPILink = "https://inspirobot.me/api?generate=true"
	cooldownSecs      = 7
)

func CommandInspirobot(discord *discordgo.Session, message *discordgo.MessageCreate, args []string) {
	// The inspirobot command, for more info see help.txt
	if !utils.CooldownIsAvailable("inspirobot", message.ChannelID) {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Please wait a few seconds before running that command again.")
		return
	}

	resp, err := http.Get(inspirobotAPILink)
	if err != nil {
		discord.ChannelMessageSend(message.ChannelID, "Error while contacting inspirobot API")
		return
	}

	link, _ := ioutil.ReadAll(resp.Body)

	imageResp, err := http.Get(string(link))

	if err != nil {
		discord.ChannelMessageSend(message.ChannelID, "Error while downloading image")
		return
	}

	discord.ChannelFileSend(message.ChannelID, "inspirobot.jpg", imageResp.Body)
	utils.CooldownAdd("inspirobot", message.ChannelID, cooldownSecs)
}
