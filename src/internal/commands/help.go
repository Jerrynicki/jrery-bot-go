package commands

import (
	"fmt"
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const (
	helpFile = "help.txt"
)

var (
	helpFileRead map[string]string
)

func CommandsHelpInit() {
	// Initializes the help command by reading the help.txt file
	raw, err := os.ReadFile(helpFile)
	if err != nil {
		fmt.Println("help.txt could not be read")
	}

	buf := strings.Split(string(raw), "\n")
	helpFileRead = make(map[string]string)

	var currentCommand string
	var splitBuf []string
	descriptionBuf := new(strings.Builder)

	for _, x := range buf {
		if currentCommand == "" {
			splitBuf = strings.Split(x, " ")
			currentCommand = splitBuf[0]

			descriptionBuf.WriteString(strings.Join(splitBuf[1:], " ") + "\n")
		} else {
			if x == "end" {
				helpFileRead[currentCommand] = descriptionBuf.String()
				descriptionBuf = new(strings.Builder)
				currentCommand = ""
			} else {
				descriptionBuf.WriteString(x + "\n")
			}
		}
	}

	fmt.Println("Documentation for", len(helpFileRead), "commands loaded")

}

func CommandHelp(discord *discordgo.Session, message *discordgo.MessageCreate, args []string) {
	// The help command, for more info see help.txt
	if len(args) == 0 {
		msg := new(strings.Builder)
		for k, v := range helpFileRead {
			msg.WriteString("**" + k + "** " + strings.Split(v, "\n")[0] + "\n")
		}
		discord.ChannelMessageSend(message.ChannelID, "Available commands:\n"+msg.String())
	} else {
		docs, contains := helpFileRead[args[0]]
		if contains {
			discord.ChannelMessageSend(message.ChannelID, "**"+args[0]+"**:\n"+docs)
		} else {
			discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Sorry, I don't have any documentation for `"+args[0]+"`")
		}
	}
}
