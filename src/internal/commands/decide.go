package commands

import (
	"math/rand"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func CommandDecide(discord *discordgo.Session, message *discordgo.MessageCreate, args []string) {
	// The decide command, for more info see help.txt

	if len(args) == 0 { // no arguments
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> I feel like you forgot something.")
	} else if len(args) > 0 { // Single number or list of poss.
		var val int64
		valtmp, err := strconv.Atoi(args[0])

		val = int64(valtmp)

		// using just the int type causes crashes on 32 bit system if the number does not fit into a 32 bit int
		// why does the atoi function not give an error when it doesn't actually fit? i don't know!

		if err != nil { // If int conversion fails, assume it is a list of possibilities separated by |
			//discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> what")
			possibilities := strings.Split(strings.Join(args, " "), "|")
			discord.ChannelMessageSend(message.ChannelID, "дщерери бот says: `"+possibilities[rand.Intn(len(possibilities))]+"`")
			return
		}

		discord.ChannelMessageSend(message.ChannelID, "дщерери бот says: `"+strconv.FormatInt(rand.Int63n(val+1), 10)+"`")
	}
}
