package commands

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

func CommandRemind(discord *discordgo.Session, message *discordgo.MessageCreate, args []string) {
	// The remind command, for more info see help.txt

	if len(args) == 0 {
		discord.ChannelMessageSend(message.ChannelID, "I think you forgot something :thinking: <@!"+message.Author.ID+">")
		return
	}

	switch args[0] {
	case "list":
		var iso string

		reminders := utils.GetRemindersFor(message.Author.ID)
		msg := new(strings.Builder)

		msg.WriteString("Reminders for **" + message.Author.Username + "**:\n")

		for i, x := range reminders {
			iso = fmt.Sprintf("<t:%d:F>", time.Unix(x.Tm, 0).Unix())
			message := fmt.Sprintf("[%s] %s (in %s): `%s`\n", strconv.Itoa(i), iso, utils.FormatDuration(int(x.Tm-time.Now().Unix())), x.Message)
			msg.WriteString(message)
		}

		if len(reminders) == 0 {
			msg.WriteString("(no reminders found)")
		}

		discord.ChannelMessageSend(message.ChannelID, msg.String())

	case "remove", "delete":
		var err error
		var index int
		reminders := utils.GetRemindersFor(message.Author.ID)

		if len(args) > 1 {
			index, err = strconv.Atoi(args[1])
		} else {
			index, err = 0, errors.New("no index supplied")
		}

		if err != nil || index > len(reminders)-1 {
			discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Are you sure you didn't mistype the index?")
			return
		}

		success := utils.RemindRemoveReminder(reminders[index])

		if !success {
			discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Sorry, I couldn't find that reminder")
			return
		}

		discord.MessageReactionAdd(message.ChannelID, message.ID, "🗑")
	default:
		tm, err := utils.ProcessTimestamp(args[0])

		if err != nil {
			discord.ChannelMessageSend(message.ChannelID, "извиняюсь, I don't know what you mean by `"+args[0]+"`")
			return
		}

		link := "https://discord.com/channels/" + message.GuildID + "/" + message.ChannelID + "/" + message.ID

		reminder := utils.Reminder{
			UserID:  message.Author.ID,
			Tm:      tm,
			Message: strings.Join(args[1:], " "),
			Link:    link,
		}

		err = utils.RemindAddReminder(reminder)

		if err != nil {
			discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Something went wrong while adding your reminder <a:peepoHehee:881101604554694676>")
			fmt.Println(err)
			return
		}

		iso := fmt.Sprintf("<t:%d:F>", time.Unix(reminder.Tm, 0).Unix())
		msg := fmt.Sprintf("<@!%s> I will remind you about `%s` on %s", message.Author.ID, reminder.Message, iso)
		discord.ChannelMessageSend(message.ChannelID, msg)

	}
}
