package commands

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

const (
	maxContentLength = 20 * 1024 * 1024 // 20 MB
	ffmpegTimeout    = 20
	cooldownLength   = 20
)

func CommandFFmpeg(discord *discordgo.Session, message *discordgo.MessageCreate, args []string) {
	if !utils.CooldownIsAvailable("ffmpeg", message.ChannelID) {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Please wait a few seconds before running that command again.")
		return
	} else if len(args) < 2 {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> I think you forgot something.")
		return
	} else if strings.Contains(strings.Join(args, ""), "-i") || strings.Contains(args[len(args)-1], "..") || strings.Contains(args[len(args)-1], "/") {
		// No -i in args, no .. or / in filetype
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> You gave an illegal argument.")
		return
	}

	utils.CooldownAdd("ffmpeg", message.ChannelID, 20)
	ffmpegLocation := utils.Config.FFmpegLocation

	resp, err := http.Get(args[0])

	if err != nil || resp.StatusCode != 200 {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> There was an error downloading the file.")
		return
	}

	if resp.ContentLength > maxContentLength {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Maximum file size is "+strconv.Itoa(maxContentLength/1024/1024)+" MB.")
		return
	}

	if err != nil {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> There was an error downloading the file.")
		return
	}

	filename := message.ChannelID + "." + args[len(args)-1]
	filepath := "cache/" + filename

	tmp := strings.Split(args[0], ".")
	inputFilename := "input_" + message.ChannelID + "." + tmp[len(tmp)-1]
	inputFilepath := "cache/" + inputFilename

	content, err := ioutil.ReadAll(resp.Body)

	defer cleanup(inputFilepath, filepath)

	if err != nil {
		discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> There was an error downloading the file.")
		return
	}

	ioutil.WriteFile(inputFilepath, content, 0644)

	cmdArgs := make([]string, 0)
	cmdArgs = append(cmdArgs, "-y", "-i", inputFilepath)
	cmdArgs = append(cmdArgs, args[1:len(args)-1]...)
	cmdArgs = append(cmdArgs, "-f", args[len(args)-1], filepath)

	var stderrBuf bytes.Buffer
	command := exec.Command(ffmpegLocation, cmdArgs...)
	command.Stderr = &stderrBuf

	command.Start()

	loadingMsg, err := discord.ChannelMessageSendReply(message.ChannelID, "🔁", message.Reference())

	if err != nil {
		return
	}

	done := make(chan error)
	go func() { done <- command.Wait() }()

	timeout := time.After(ffmpegTimeout * time.Second)

	select {
	case <-timeout:
		command.Process.Kill()
		discord.ChannelMessageEdit(loadingMsg.ChannelID, loadingMsg.ID, "FFmpeg command timed out.")
	case err := <-done:
		if err != nil {
			stderr := make([]byte, stderrBuf.Len())
			stderrBuf.Read(stderr)
			discord.ChannelFileSendWithMessage(message.ChannelID, "ffmpeg brokey <a:peepoWeirdLeave:654017116055535664>", "ffmpeg.log", bytes.NewReader(stderr))
			return
		}

		discord.ChannelMessageDelete(loadingMsg.ChannelID, loadingMsg.ID)
		file, err := os.Open(filepath)
		if err != nil {
			return
		}
		discord.ChannelFileSendWithMessage(message.ChannelID, "<@!"+message.Author.ID+">", filename, file)
	}
}

func cleanup(inputFilepath string, filepath string) {
	// Cleans up the input file, output file
	os.Remove(inputFilepath)
	os.Remove(filepath)
}
