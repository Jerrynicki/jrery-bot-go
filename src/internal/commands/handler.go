package commands

import (
	"strings"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

func MessageCreate(discord *discordgo.Session, message *discordgo.MessageCreate) {
	// MessageCreate event, handles everything having to do with messages including command processing
	if message.Author.ID == discord.State.User.ID || message.Author.Bot {
		return
	}

	if strings.Contains(message.Content, "<@!"+discord.State.User.ID+">") || strings.Contains(message.Content, "<@"+discord.State.User.ID+">") {
		go discord.ChannelMessageSend(message.ChannelID, "Добрый день, друг мой!")
	}

	if strings.HasPrefix(message.Content, utils.Config.Prefix) {
		buf := strings.Split(message.Content[len(utils.Config.Prefix):], " ")
		command := buf[0]
		args := buf[1:]

		switch command {
		case "ping":
			go CommandPing(discord, message, args)
		case "remind":
			go CommandRemind(discord, message, args)
		case "inspirobot":
			go CommandInspirobot(discord, message, args)
		case "help":
			go CommandHelp(discord, message, args)
		case "decide":
			go CommandDecide(discord, message, args)
		case "ffmpeg":
			go CommandFFmpeg(discord, message, args)
		default:
			if len(command) == 0 {
				go discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> Вы забыли команду")
			} else {
				go discord.ChannelMessageSend(message.ChannelID, "<@!"+message.Author.ID+"> извиняюсь, I don't know any command by the name of `"+command+"`")
			}
		}
	}
}
