package utils

import (
	"encoding/gob"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
)

const (
	remindersFile            = "user_data/reminders.gob"
	remindersFilePermissions = 0644
	remindLoopSleepDuration  = 10
)

type Reminder struct {
	UserID  string
	Tm      int64
	Message string
	Link    string
}

var (
	reminders     []Reminder
	remindersLock sync.Mutex
)

func RemindInit(discord *discordgo.Session) {
	// Initializes the reminders
	var err error

	reminders, err = remindLoadReminders()
	if err != nil {
		panic(err)
	}

	fmt.Println("Reminder initialized!!! :))))))) (loaded", len(reminders), "reminders)")

	go RemindLoop(discord)
}

func RemindLoop(discord *discordgo.Session) {
	// Should be called as a goroutine
	// Handles everything related to reminders

	var tm int64

	var reminder Reminder
	var i int

	var toRemove []int

	var err error
	var gotErr bool
	var ch *discordgo.Channel
	var user *discordgo.User
	var msg string

	for {
		remindersLock.Lock()

		// Loop through slice of reminders, if one is due,
		// create a channel with the user and send them the reminder.
		// After that, queue the reminder for deletion
		tm = time.Now().Unix()
		for i, reminder = range reminders {
			if reminder.Tm < tm {
				ch, err = discord.UserChannelCreate(reminder.UserID)
				gotErr = err != nil
				user, err = discord.User(reminder.UserID)
				gotErr = gotErr || err != nil

				if !gotErr {
					msg = "Hey " + user.Username + "!\nYou wanted me to remind you about `" + reminder.Message + "`!\n\n" + reminder.Link
					go discord.ChannelMessageSend(ch.ID, msg)
				} else {
					fmt.Println(err)
				}

				toRemove = append(toRemove, i)
			}
		}

		if len(toRemove) > 0 {
			// Make new slice the size of reminders-toRemove, then iterate over
			// reminders, adding every element that is not contained in toRemove to the
			// new slice
			remindersCpy := make([]Reminder, 0, len(reminders)-len(toRemove))
			for i, x := range reminders {
				if !Contains(i, toRemove) {
					remindersCpy = append(remindersCpy, x)
				}
			}

			// Replace the old slice
			reminders = remindersCpy

			// Update the database on disk
			err = remindSaveReminders(reminders)
			if err != nil {
				fmt.Println(err)
			}
		}

		// Reset the reminder lock and toRemove
		remindersLock.Unlock()
		toRemove = make([]int, 0)

		time.Sleep(remindLoopSleepDuration * time.Second)
	}
}

func remindLoadReminders() ([]Reminder, error) {
	// Loads reminders from a json file
	var rmndrs []Reminder

	file, err := os.Open(remindersFile)
	if err != nil {
		return nil, err
	}

	decoder := gob.NewDecoder(file)
	decoder.Decode(&rmndrs)
	file.Close()

	return rmndrs, nil
}

func remindSaveReminders(rmndrs []Reminder) error {
	// Saves reminders to a json file
	file, err := os.Create(remindersFile)
	if err != nil {
		return err
	}

	encoder := gob.NewEncoder(file)
	encoder.Encode(rmndrs)
	file.Close()

	return nil
}

func RemindAddReminder(reminder Reminder) error {
	// Adds a reminder
	remindersLock.Lock()
	reminders = append(reminders, reminder)
	remindSaveReminders(reminders)
	remindersLock.Unlock()

	return nil
}

func RemindRemoveReminder(rmndr Reminder) bool {
	// Removes a reminder and returns whether it was found
	var removed bool

	remindersLock.Lock()
	for i, x := range reminders {
		if x == rmndr {
			reminders = append(reminders[:i], reminders[i+1:]...)
			removed = true
			break
		}
	}

	remindSaveReminders(reminders)
	remindersLock.Unlock()

	return removed
}

func GetRemindersFor(userID string) []Reminder {
	// Returns an array of reminders for given userID

	result := make([]Reminder, 0)

	remindersLock.Lock()
	for _, x := range reminders {
		if x.UserID == userID {
			result = append(result, x)
		}
	}
	remindersLock.Unlock()
	return result
}
