package utils

import (
	"sync"
	"time"
)

var (
	cooldowns     map[[2]string]int64 = make(map[[2]string]int64)
	cooldownsLock sync.Mutex
)

func CooldownAdd(command string, channelID string, seconds int) {
	// Adds a cooldown for command in channelID for seconds seconds
	cooldownsLock.Lock()
	cooldowns[[2]string{command, channelID}] = time.Now().Unix() + int64(seconds)
	cooldownsLock.Unlock()
}

func CooldownIsAvailable(command string, channelID string) bool {
	cooldownsLock.Lock()
	tm, inMap := cooldowns[[2]string{command, channelID}]
	cooldownsLock.Unlock()

	if inMap {
		return tm < time.Now().Unix()
	}
	return true
}
