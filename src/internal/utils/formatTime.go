package utils

import (
    "strings"
    "strconv"
    "time"
    "errors"
)

func ProcessTimestamp(timeHumanReadable string) (int64, error) {
    // Takes human-readable timetamp like "2d2h" (2 days, 2 hours)
    // and gives the resulting timestamp

    // FIXME: Maybe switch to time.ParseDuration in the future, but that doesn't support d for days

    var result int
    var numBuf []rune
    var buf int
    var multiplyBy int

    numbers := []rune{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

    for _, x := range timeHumanReadable {
        if Contains(x, numbers) { // If x is a number, add it to the num buffer
            numBuf = append(numBuf, x)
        } else { // If x is not a number, it must be a character referring to length, like s, m, h, d for sec, mins, hours, days -> Multiply numbuf with that and add it to total len
            switch x {
                case 's':
                    multiplyBy = 1
                case 'm':
                    multiplyBy = 60
                case 'h':
                    multiplyBy = 3600
                case 'd':
                    multiplyBy = 86400
                default:
                    multiplyBy = 0 // invalid value will be ignored
            }

            buf, _ = strconv.Atoi(string(numBuf)) // error can be safely ignored since only numbers are able to make it into the numBuf slice
            result += buf * multiplyBy

            numBuf = make([]rune, 0)
        }
    }

    if result == 0 {
        return 0, errors.New("time adds to zero")
    } else {
        return time.Now().Unix() + int64(result), nil
    }
}

func FormatDuration(duration int) string {
    var vals [4]int // days, hours, minutes, seconds
    result := new(strings.Builder)

    vals[0] = duration / 86400
    duration -= vals[0] * 86000
    vals[1] = duration / 3600
    duration -= vals[1] * 3600
    vals[2] = duration / 60
    duration -= vals[2] * 60
    vals[3] = duration

    for i, x := range vals {
        if x != 0 {
            switch i {
                case 0:
                    result.WriteString(strconv.Itoa(x) + "d")
                case 1:
                    result.WriteString(strconv.Itoa(x) + "h")
                case 2:
                    result.WriteString(strconv.Itoa(x) + "m")
                case 3:
                    result.WriteString(strconv.Itoa(x) + "s")
            }
        }
    }

    return result.String()

}
