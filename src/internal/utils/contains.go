package utils

import (
	"reflect"
)

func Contains(elem interface{}, sliceInterface interface{}) bool {
	// Returns true if sliceInterface contains elem
	slice := reflect.ValueOf(sliceInterface)

	for i := 0; i < slice.Len(); i++ {
		if elem == slice.Index(i).Interface() {
			return true
		}
	}

	return false
}
