package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type ConfigStruct struct {
	Token          string
	Prefix         string
	FFmpegLocation string
}

const (
	configLocation = "config.json"
)

var (
	Config ConfigStruct
)

func ReadConfig() {
	// Read json config
	data, err := ioutil.ReadFile(configLocation)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &Config)
	if err != nil {
		panic(err)
	}

	fmt.Println("Read config!!! :)))")
}
