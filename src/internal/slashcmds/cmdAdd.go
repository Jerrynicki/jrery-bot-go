package slashcmds

import (
    "github.com/bwmarrin/discordgo"
)

func addCmds(c []*discordgo.ApplicationCommand, h CommandHandlerMap) ([]*discordgo.ApplicationCommand, CommandHandlerMap) {
    c = append(c, &discordgo.ApplicationCommand{
        Name:        "test",
        Description: "testi test",
    })
    h["test"] = SlashCmdTest

    c = append(c, &discordgo.ApplicationCommand{
        Name:        "remind",
        Description: "Commands related to reminders",
        Options: []*discordgo.ApplicationCommandOption{
            {
                Type:        discordgo.ApplicationCommandOptionSubCommand,
                Name:        "reminder",
                Description: "Set a reminder",
                Options: []*discordgo.ApplicationCommandOption{
                    {
                        Type:        discordgo.ApplicationCommandOptionString,
                        Name:        "in",
                        Description: "How much time until the reminder should go off",
                        Required:    true,
                    },
                    {
                        Type:        discordgo.ApplicationCommandOptionString,
                        Name:        "reminder",
                        Description: "What would you like to be reminded about?",
                        Required:    true,
                    },
                    {
                        Type:        discordgo.ApplicationCommandOptionBoolean,
                        Name:        "private",
                        Description: "Whether you want this interaction to be private",
                        Required:    false,
                    },
                },
            },
            {
                Type:        discordgo.ApplicationCommandOptionSubCommand,
                Name:        "list",
                Description: "Get a list of your reminders",
                Options: []*discordgo.ApplicationCommandOption{
                    {
                        Type:        discordgo.ApplicationCommandOptionBoolean,
                        Name:        "private",
                        Description: "Whether you want this interaction to be private",
                        Required:    false,
                    },
                },
            },
        },
    })
    h["remind"] = SlashCmdRemind

    return c, h
}
