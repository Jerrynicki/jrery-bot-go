package slashcmds

import (
    "github.com/bwmarrin/discordgo"
)

func SlashCmdTest(discord *discordgo.Session, interaction *discordgo.InteractionCreate) {
    discord.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
        Type: discordgo.InteractionResponseChannelMessageWithSource,
        Data: &discordgo.InteractionResponseData{
            Content: "test",
        },
    })
}
