package slashcmds

import (
    "fmt"

    "github.com/bwmarrin/discordgo"
)

type CommandHandlerMap map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)

const (
    guildID = ""
)

var (
    ready    bool
    commands []*discordgo.ApplicationCommand
    handlers CommandHandlerMap
)

func RegisterSlashCmds(discord *discordgo.Session) {
    // Registers slash commands, must be run AFTER user ID is initialized
    commands = make([]*discordgo.ApplicationCommand, 0)
    handlers = make(CommandHandlerMap)
    commands, handlers = addCmds(commands, handlers)

    for _, c := range commands {
        _, err := discord.ApplicationCommandCreate(discord.State.User.ID, guildID, c)
        if err != nil {
            fmt.Println("Error while registering slash command", c.Name, err)
        }
    }

    fmt.Println("Registered", len(commands), "slash commands")

    ready = true
}

func UnregisterAllSlashCmds(discord *discordgo.Session) {
    // delete all global commands
    cmds, err := discord.ApplicationCommands(discord.State.User.ID, "")
    if err != nil {
        fmt.Println("Error getting global commands", err)
    } else {
        for _, cmd := range cmds {
            discord.ApplicationCommandDelete(discord.State.User.ID, "", cmd.ID)
            fmt.Println("Deleted global command", cmd.Name)
        }
    }

    // delete all guild-specific commands
    for _, guild := range discord.State.Guilds {
        cmds, err := discord.ApplicationCommands(discord.State.User.ID, guild.ID)
        if err != nil {
            fmt.Println("Error getting commands for", guild.Name, err)
        } else {
            for _, cmd := range cmds {
                discord.ApplicationCommandDelete(discord.State.User.ID, guild.ID, cmd.ID)
                fmt.Println("Deleted", cmd.Name, "on", guild.Name)
            }
        }
    }
}

func InteractionCreate(discord *discordgo.Session, interaction *discordgo.InteractionCreate) {
    if !ready {
        return
    }

    if handler, contains := handlers[interaction.ApplicationCommandData().Name]; contains {
        go handler(discord, interaction)
    }
}
