package slashcmds

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/Jerrynicki/jrery-bot-go/src/internal/utils"
)

func SlashCmdRemind(discord *discordgo.Session, interaction *discordgo.InteractionCreate) {
	var userID string
	if interaction.User != nil {
		userID = interaction.User.ID
	} else if interaction.Member != nil {
		userID = interaction.Member.User.ID
	} else {
		discord.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "what the fuck",
			},
		})
		return
	}

	data := interaction.ApplicationCommandData()

	switch data.Options[0].Name {
	case "reminder":
		var flags discordgo.MessageFlags

		if len(data.Options[0].Options) == 3 { // private defined
			if data.Options[0].Options[2].BoolValue() {
				flags = 1 << 6
			} else {
				flags = 0
			}
		}

		tm, err := utils.ProcessTimestamp(data.Options[0].Options[0].StringValue())
		msg := data.Options[0].Options[1].StringValue()

		if err != nil {
			discord.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Invalid time",
					Flags:   flags,
				},
			})
			return
		}

		link := "https://discord.com/channels/" + interaction.GuildID + "/" + interaction.ChannelID + "/" + interaction.ID

		reminder := utils.Reminder{
			UserID:  userID,
			Tm:      tm,
			Message: msg,
			Link:    link,
		}

		err = utils.RemindAddReminder(reminder)

		if err != nil {
			discord.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Error adding reminder",
					Flags:   flags,
				},
			})
			return
		}

		iso := fmt.Sprintf("<t:%d:F>", time.Unix(reminder.Tm, 0).Unix())
		msgString := fmt.Sprintf("I will remind you about `%s` on %s", reminder.Message, iso)
		discord.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: msgString,
				Flags:   flags,
			},
		})
	case "list":
		var iso string
		var flags discordgo.MessageFlags

		if len(data.Options[0].Options) == 1 { // private defined
			if data.Options[0].Options[0].BoolValue() {
				flags = 1 << 6
			} else {
				flags = 0
			}
			fmt.Println(data.Options[0].Options[0].BoolValue())
		}

		reminders := utils.GetRemindersFor(userID)
		msg := new(strings.Builder)

		for i, x := range reminders {
			iso = fmt.Sprintf("<t:%d:F>", time.Unix(x.Tm, 0).Unix())
			message := fmt.Sprintf("[%s] %s (in %s): `%s`\n", strconv.Itoa(i), iso, utils.FormatDuration(int(x.Tm-time.Now().Unix())), x.Message)
			msg.WriteString(message)
		}

		if len(reminders) == 0 {
			msg.WriteString("(no reminders found)")
		}

		discord.InteractionRespond(interaction.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: msg.String(),
				Flags:   flags,
			},
		})

	}
}
