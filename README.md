# jrery-bot-go

**This project is a Go rewrite of the most widely used features from [jrery bot](https://github.com/Jerrynicki/jrery-bot)**, [see below](#features) for more info

## Installing

- *Note: If you just want to use the bot, there will be a public instance added here once some more progress is done*

### Method 1: Compiling

- Clone the repository

- Install [discordgo](https://github.com/bwmarrin/discordgo)

- [Create a discord bot account](https://discordpy.readthedocs.io/en/stable/discord.html) and get the token

- Run the `build.sh` file or (on Windows) create a bin directory and run `go build -o bin/jrery-bot-go src/*.go` in the root directory

- Run the `install.sh` file or (on Windows) create the config.json and user_data structure yourself

- Run the bot by running the `jrery-bot-go` executable

### Method 2: Release

- Download the correct release binary for your platform from [the releases page](https://gitlab.com/Jerrynicki/jrery-bot-go/-/releases)

- [Create a discord bot account](https://discordpy.readthedocs.io/en/stable/discord.html) and get the token

- Also download the [install.sh](https://gitlab.com/Jerrynicki/jrery-bot-go/-/raw/main/install.sh?inline=false)

- Put both together in a directory and run the install.sh file or (on Windows) create the config.json and user_data structure yourself

- Run the bot by running the `jrery-bot-go` executable

### Features

#### Implemented

✅ Help command

✅ Reminders

✅ Inspirobot command

✅ Ping command (the most important one)

✅ Decide command

✅ FFmpeg command

#### Planned

⏳ Streets command [(?)](https://github.com/Jerrynicki/jrery-bot/blob/master/commands/streets.py)

⏳ Some game commands

⏳ Whatever comes to mind in the future

*Licensed under the MIT License*